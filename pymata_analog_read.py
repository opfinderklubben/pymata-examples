import time
import signal
import sys

from PyMata.pymata import PyMata

# Create a board. No bluetooth = faster board start up
board = PyMata("/dev/ttyUSB0", bluetooth=False, verbose=True)

# Pin 13 is connected to an on board LED
LED_PIN = 13

# Pin with analog (Variabel voltage output) sensor attached
SENSOR_PIN = 2 # A2 on Arduino Nano

# Turn on LED if sensor reding is above this value
THRESHOLD = 600

def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    if board is not None:
        board.reset()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Configure the LED_PIN as a digital output
board.set_pin_mode(LED_PIN, board.OUTPUT, board.DIGITAL)

# Configure the SENSOR_PIN as an analog input
board.set_pin_mode(SENSOR_PIN, board.INPUT, board.ANALOG)


# Do this forever (True is always True)
while True:

    # Read voltage on SENSOR_PIN
    sensor_reading = board.analog_read(SENSOR_PIN)

    # Print out sensor reading
    print("Sensor value:", sensor_reading)

    # Turn on LED if sensor reading is above threshold
    if sensor_reading > THRESHOLD:
        board.digital_write(LED_PIN, 1);
    else:
        board.digital_write(LED_PIN, 0);
        
    # Wait 0.5 seconds
    time.sleep(0.2)
