import time
import signal
import sys

from PyMata.pymata import PyMata

# Create a board. No bluetooth = faster board start up
board = PyMata("/dev/ttyUSB0", bluetooth=False, verbose=True)

# Pin 13 is connected to an on board LED 
LED_PIN = 13

# Define ON and OFF for easier to read code
ON = 1
OFF = 0

def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    if board is not None:
        board.reset()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Configure the LED_PIN as a digital output
board.set_pin_mode(LED_PIN, board.OUTPUT, board.DIGITAL)


# Do this forever (True is always True)
while True:

    # Turn on pin with LED
    board.digital_write(LED_PIN, ON)
    # Wait 0.5 seconds
    time.sleep(0.5)
    
    # Turn off pin with LED
    board.digital_write(LED_PIN, OFF)
    # Wait 0.5 seconds
    time.sleep(0.5)
    
