# PyMata examples

A collection of PyMata examples to get you started interacting with microprocessors running Firmata.

They have been tested on GNU/Linux, but should work anywhere Python3 and PyMata is installed.

(Read about Firmata)[https://github.com/firmata/protocol]
(Read about PyMata)https://github.com/MrYsLab/PyMata
## Examples ##

- You can run the examples like this ''python3 pymata_example.py''
- Change the port in the code, if you need something other than ''/dev/ttyUSB0''

When you run an example programme, you should see something like this:
```
PyMata version 2.20  Copyright(C) 2013-19 Alan Yorinks    All rights reserved.

Opening Arduino Serial port /dev/ttyUSB0 

Please wait while Arduino is being detected. This can take up to 30 seconds ...
Board initialized in 1 seconds
Total Number of Pins Detected = 22
Total Number of Analog Pins Detected = 8
```

### pymata_flash_led.py ###
This programme flashes the onboard LED of Arduino Nano (All?) boards connected to pin 13.
Change the variable **LED_PIN** to use a different (digital) pin. 

## Installing Firmata ##
How to get the Firmata firmware on to your microprocessor.

### Arduino Nano ##
How I have flashed chinese Arduino Nano clones from within the Arduino IDE.

1. Connect Arduino Nano with USB cable.
2. Open Arduino IDE
3. File -> Examples -> Firmata -> StandardFirmata
4. Tools -> Board -> Arduino Nano
5. Tools -> Processor -> ATmega328P (Old Bootloader)
6. Tools -> Port -> /dev/ttyUSB0 (You might have another path here)
7. Sketch -> Upload
