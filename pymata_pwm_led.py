import time
import signal
import sys

from PyMata.pymata import PyMata

# Pins with PWM on Arduino Nano are: 3,5,6,9,10,11
PWM_LED_PIN = 6

# The maximim duty cycle to use for PWM
MAX_INTENSITY = 130

# Create a board. No bluetooth = faster board start up
board = PyMata("/dev/ttyUSB0", bluetooth=False, verbose=True)

def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    if board is not None:
        board.reset()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Configure the PWM_LED_PIN for PWM
board.set_pin_mode(PWM_LED_PIN, board.PWM, board.DIGITAL)

# List of values for PWM duty cycle to run through
ramp_up = list(range(MAX_INTENSITY))
ramp_down = list(reversed(ramp_up))

# Do this forever (True is always True)
while True:

    for i in ramp_up + ramp_down:
        board.analog_write(PWM_LED_PIN, i)
        # Wait 0.01 seconds
        time.sleep(0.01)
