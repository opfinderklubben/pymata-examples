import time
import signal
import sys

from PyMata.pymata import PyMata

# Create a board. No bluetooth = faster board start up
board = PyMata("/dev/ttyUSB0", bluetooth=False, verbose=True)

# Pins to use with LEDs 
PINS = [2,3,4,5,6,7,8,9]

# Animation to show on the LEDs
PATTERN = [
  [1,0,0,0,0,0,0,0],
  [0,1,0,0,0,0,0,0],
  [0,0,1,0,0,0,0,0],
  [0,0,0,1,0,0,0,0],
  [0,0,0,0,1,0,0,0],
  [0,0,0,0,0,1,0,0],
  [0,0,0,0,0,0,1,0],
  [0,0,0,0,0,0,0,1],
  [0,0,0,0,0,0,1,0],
  [0,0,0,0,0,1,0,0],
  [0,0,0,0,1,0,0,0],
  [0,0,0,1,0,0,0,0],
  [0,0,1,0,0,0,0,0],
  [0,1,0,0,0,0,0,0],
  ]

# Lower number = faster speed
SPEED = 0.05

def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    if board is not None:
        board.reset()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Configure the LED pins as a digital outputs
for pin_no in PINS:
    board.set_pin_mode(pin_no, board.OUTPUT, board.DIGITAL)


# Do this forever (True is always True)
while True:

    # Run through the animation
    for led_states in PATTERN:

        # Set state for all LEDs
        for i in range(len(PINS)):
            board.digital_write(PINS[i], led_states[i])

        # Pause
        time.sleep(SPEED)
